require("expose-loader?$!expose-loader?jQuery!jquery")
require("bootstrap/dist/js/bootstrap.bundle.js")

import Vue from "vue"
import VueRouter from "router"
Vue.use(VueRouter)

import App from "./App.vue"
import BandComponent from "./components/band.vue"
import MembersComponent from "./components/members.vue"

const routes = [
  {
    path: '/', component: App, redirect: '/list', children: [
      { path: '/band/:id', name: 'showBand', component: MembersComponent },
      { path: '/list', component: BandComponent }
    ]
  }
]

const router = new VueRouter({
  mode: "history",
  routes
})

const app = new Vue({
  router
}).$mount("#app")
