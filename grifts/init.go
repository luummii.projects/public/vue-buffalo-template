package grifts

import (
	"projects/vue-buffalo-template/actions"

	"github.com/gobuffalo/buffalo"
)

func init() {
	buffalo.Grifts(actions.App())
}
